<?php

/**
 * File called to bootstrap the application
 *
 * Autoloads classes and configures application
 */

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
date_default_timezone_set('UTC');

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Gaw508\Config\Config;

// Configuration of directories
Config::set('ROOT_DIR', dirname(__DIR__));
Config::set('CONFIG_DIR', Config::get('ROOT_DIR') . '/config');
Config::set('TEMPLATE_DIR', Config::get('ROOT_DIR') . '/frontend/templates');
Config::set('LOG_DIR', Config::get('ROOT_DIR') . '/tmp/log');

// Load default config
Config::loadYaml(__DIR__ . '/defaults.yml');

// Autoload other config
Config::loadDirectory(__DIR__ . '/autoload');
