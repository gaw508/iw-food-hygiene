Feature: Rating distributions
    As a user I want to see how food hygiene ratings are distributed by
    percentage across a selected Local Authority so that I can understand the
    profile of establishments in that authority.

    Rules:
    - In all regions except Scotland, possible ratings include "Exempt",
      "Awaiting Inspection", "Awaiting Publication" and 0 - 5.
    - In Scotland, possible ratings include "Exempt", "Pass", "Pass and Eat
      Safe", "Improvement Required", "Awaiting Inspection"
    - Distributions should be given as a percentage of the total number of
      establishments in a given local authority.

    Scenario: Choosing a local authority outside Scotland with some establishments at every rating.
      Given there is a local authority "Ipswich", with ID 101, Scheme Type 1 and with "100 fhrs_exempt_en-gb, 50 fhrs_5_en-gb, 50 fhrs_4_en-gb, 200 fhrs_3_en-gb, 150 fhrs_2_en-gb, 100 fhrs_1_en-gb, 50 fhrs_0_en-gb, 40 fhrs_awaitinginspection_en-gb, 10 fhrs_awaitingpublication_en-gb"
      When I select the local authority "Ipswich", with ID 101
      Then the distribution will be "13.3 Exempt, 6.7 5-star, 6.7 4-star, 26.7 3-star, 20 2-star, 13.3 1-star, 6.7 0-star, 5.3 Awaiting Inspection, 1.3 Awaiting Publication"

    Scenario: Choosing a local authority outside Scotland with no establishments in some rating.
      Given there is a local authority "Ipswich", with ID 101, Scheme Type 1 and with "100 fhrs_exempt_en-gb, 50 fhrs_5_en-gb, 0 fhrs_4_en-gb, 200 fhrs_3_en-gb, 0 fhrs_2_en-gb, 100 fhrs_1_en-gb, 50 fhrs_0_en-gb, 100 fhrs_awaitinginspection_en-gb, 0 fhrs_awaitingpublication_en-gb"
      When I select the local authority "Ipswich", with ID 101
      Then the distribution will be "16.7 Exempt, 8.3 5-star, 0 4-star, 33.3 3-star, 0 2-star, 16.7 1-star, 8.3 0-star, 16.7 Awaiting Inspection, 0 Awaiting Publication"

    Scenario: Choosing a local authority within Scotland with some establishments at every rating.
      Given there is a local authority "Glasgow", with ID 102, Scheme Type 2 and with "100 fhis_exempt_en-gb, 50 fhis_pass_en-gb, 50 fhis_pass_and_eat_safe_en-gb, 200 fhis_improvement_required_en-gb, 25 fhis_awaiting_inspection_en-gb, 25 fhis_awaiting_publication_en-gb"
      When I select the local authority "Glasgow", with ID 102
      Then the distribution will be "22.2 Exempt, 11.1 Pass, 11.1 Pass and Eat Safe, 44.4 Improvement Required, 5.6 Awaiting Inspection, 5.6 Awaiting Publication"

    Scenario: Choosing a local authority within Scotland with no establishments in some rating.
      Given there is a local authority "Glasgow", with ID 102, Scheme Type 2 and with "100 fhis_exempt_en-gb, 50 fhis_pass_en-gb, 0 fhis_pass_and_eat_safe_en-gb, 0 fhis_improvement_required_en-gb, 50 fhis_awaiting_inspection_en-gb, 0 fhis_awaiting_publication_en-gb"
      When I select the local authority "Glasgow", with ID 102
      Then the distribution will be "50 Exempt, 25 Pass, 0 Pass and Eat Safe, 0 Improvement Required, 25 Awaiting Inspection, 0 Awaiting Publication"
