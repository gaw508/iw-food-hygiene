<?php

date_default_timezone_set('utc');

use Behat\Behat\Context\Context;
use FoodHygiene\Model\LocalAuthority;
use FoodHygiene\FsaApiClient;
use Psr\Container\ContainerInterface as Container;
use Monolog\Logger;
use FoodHygiene\Cache\CacheFactory;
use Gaw508\Config\Config;

/**
 * Behat Behaviour Driven Development Tests - FeatureContext
 *
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var LocalAuthority
     */
    private $local_authority;

    /**
     * @var FsaApiClient
     */
    private $mock_api_client;

    /**
     * Behat "Given" step for creating a local authority
     *
     * @Given there is a local authority :localAuthority, with ID :id, Scheme Type :schemeType and with :ratings
     */
    public function thereIsALocalAuthorityWith(
        $local_authority,
        $local_authority_id,
        $scheme_type,
        $ratings
    ) {
        $rating_establishment_count = $this->parseRatings($ratings);

        $mock_generator = new PHPUnit_Framework_MockObject_Generator();
        $this->mock_api_client = $mock_generator->getMock(
            'FsaApiClient',
            array(
                'getLocalAuthorityList',
                'getLocalAuthorityByID',
                'getEstablishmentsByLocalAuthority'
            )
        );

        $this->mock_api_client
            ->method('getLocalAuthorityByID')
            ->with($local_authority_id)
            ->willReturn(array(
                "LocalAuthorityId" => $local_authority_id,
                "Name" => $local_authority,
                "SchemeType" => $scheme_type
            ));

        $this->mock_api_client
            ->method('getEstablishmentsByLocalAuthority')
            ->with($local_authority_id)
            ->willReturn($this->ratingEstablishmentCountToEstablishmentList(
                $rating_establishment_count
            ));

        $this->container = new \Slim\Container();
        $this->container['logger'] = new Logger('BehatTests');
        Config::set('cache_expiry_days', 1);
        $this->container['cache'] = CacheFactory::createCache(
            'NullCache',
            $this->container
        );
        $this->container['fsa_api_client'] = $this->mock_api_client;
        $this->local_authority = new LocalAuthority($this->container);
    }

    /**
     * Behat "When" step for selecting a authority
     *
     * @When I select the local authority :localAuthority, with ID :id
     */
    public function iSelectLocalAuthority($local_authority, $local_authority_id)
    {
        $result = $this
            ->local_authority
            ->loadByID($local_authority_id);
        PHPUnit_Framework_Assert::assertTrue($result);
    }

    /**
     * Behat "Then" step for getting a distribution
     *
     * @Then the distribution will be :distribution
     */
    public function theDistributionWillBe($expected_distribution)
    {
        $expected_distribution = $this->parseRatings($expected_distribution);
        $actual_distribution = $this->local_authority->getRatingDistribution();
        PHPUnit_Framework_Assert::assertEquals(
            $expected_distribution,
            $actual_distribution
        );
    }

    /**
     * Parse a string of ratings from a scenario and return
     * an array of key value pairs
     *
     * @param string $ratings_string    The string of ratings
     * @return array                    The list of key value pairs
     */
    private function parseRatings($ratings_string)
    {
        $ratings = explode(',', $ratings_string);
        $return = array();
        foreach ($ratings as $rating) {
            $rating = explode(' ', trim($rating));
            $value = (float) array_shift($rating);
            $return[implode(' ', $rating)] = $value;
        }
        return $return;
    }

    /**
     * Take a list of ratings and number of establishments
     * within each, and turn it into a list of establishments
     * each with it's own rating. I.e. the output you would
     * get from the API.
     *
     * @param array $ratings    Array of ratings, with count
     *                          of establishments in each.
     * @return array            Array of establishments
     */
    private function ratingEstablishmentCountToEstablishmentList($ratings)
    {
        $establishments = array();
        foreach ($ratings as $rating => $count) {
            $establishments = array_merge(
                $establishments,
                array_fill(0, $count, array(
                    "RatingKey" => $rating
                ))
            );
        }
        return $establishments;
    }
}
