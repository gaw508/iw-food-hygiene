<?php

namespace FoodHygiene;

use \Gaw508\Config\Config;
use \GuzzleHttp\Client;
use \GuzzleHttp\Psr7\Response;
use \Monolog\Handler\NullHandler;
use \Monolog\Logger;
use \PHPUnit\Framework\TestCase;
use \Slim\Container;
use \ReflectionClass;

/**
 * Class FsaApiClientTest
 * @package FoodHygiene
 */
class FsaApiClientTest extends TestCase
{
    /**
     * @var FsaApiClient
     */
    private $api_client;

    /**
     * @var mixed
     */
    private $guzzle_mock;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $container = new Container();

        $logger = new Logger('TestLogger');
        $logger->pushHandler(new NullHandler());
        $container['logger'] = $logger;

        Config::set('fsa_api_base_uri', 'http://localhost');
        Config::set('fsa_api_timeout', 15);
        Config::set('fsa_api_version', 2);
        $this->api_client = new FsaApiClient($container);

        $this->guzzle_mock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->setMethods(array('get'))
            ->getMock();

        // Change private property for guzzle client
        $reflection = new ReflectionClass($this->api_client);
        $property = $reflection->getProperty('guzzle');
        $property->setAccessible(true);
        $property->setValue($this->api_client, $this->guzzle_mock);
    }

    /**
     * Test getting a list of local authorities
     */
    public function testGetLocalAuthorityListSuccess()
    {
        $data = array(
            'authorities' => array(
                array(
                    'Name' => 'Ipswich',
                    'LocalAuthorityId' => 101,
                    'SchemeType' => 1
                ),
                array(
                    'Name' => 'Glasgow',
                    'LocalAuthorityId' => 102,
                    'SchemeType' => 2
                )
            )
        );

        $this->guzzle_mock
            ->expects($this->once())
            ->method('get')
            ->with('/Authorities/basic', array())
            ->willReturn(new Response(
                200,
                array(),
                json_encode($data)
            ));

        $list = $this->api_client->getLocalAuthorityList();
        $this->assertEquals($data['authorities'], $list);
    }

    /**
     * Test getting a list of local authorities
     */
    public function testGetLocalAuthorityListFailure()
    {
        $this->guzzle_mock
            ->expects($this->once())
            ->method('get')
            ->with('/Authorities/basic', array())
            ->willReturn(new Response(
                500,
                array(),
                ''
            ));

        $this->assertEquals(false, $this->api_client->getLocalAuthorityList());
    }

    /**
     * Test getting a local authority by id
     */
    public function testGetLocalAuthorityByIDSuccess()
    {
        $data = array(
            'Name' => 'Ipswich',
            'LocalAuthorityId' => 101,
            'SchemeType' => 1
        );

        $this->guzzle_mock
            ->expects($this->once())
            ->method('get')
            ->with('/Authorities/101', array())
            ->willReturn(new Response(
                200,
                array(),
                json_encode($data)
            ));

        $this->assertEquals(
            $data,
            $this->api_client->getLocalAuthorityByID(101)
        );
    }

    /**
     * Test getting a local authority by id
     */
    public function testGetLocalAuthorityByIDFailure()
    {
        $this->guzzle_mock
            ->expects($this->once())
            ->method('get')
            ->with('/Authorities/101', array())
            ->willReturn(new Response(
                500,
                array(),
                ''
            ));

        $this->assertFalse(
            $this->api_client->getLocalAuthorityByID(101)
        );
    }

    /**
     * Test getting list of establishements for an authority
     */
    public function testGetEstablishmentsByLocalAuthority()
    {
        $data = array(
            'establishments' => array(
                array('ratingKey' => 'fhis_exempt_en-gb', 'ratingValue' => 'Exempt'),
                array('ratingKey' => 'fhis_pass_en-gb', 'ratingValue' => 'Pass')
            ),
            'meta' => array(
                'totalPages' => 1
            )
        );

        $this->guzzle_mock
            ->expects($this->once())
            ->method('get')
            ->with('/Establishments', array(
                'query' => array(
                    'localAuthorityId' => 102,
                    'pageSize' => 0,
                    'pageNumber' => 1
                )
            ))
            ->willReturn(new Response(
                200,
                array(),
                json_encode($data)
            ));

        $this->assertEquals(
            $data['establishments'],
            $this->api_client->getEstablishmentsByLocalAuthority(102)
        );
    }
}
