<?php

namespace FoodHygiene\Cache;

use \PHPUnit\Framework\TestCase;
use \Slim\Container;

/**
 * Class NullCacheTest
 * @package FoodHygiene\Cache
 */
class NullCacheTest extends TestCase
{
    /**
     * @var NullCache
     */
    private $cache;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $container = new Container();
        $this->cache = CacheFactory::createCache('NullCache', $container);
    }

    /**
     * Test the exists function
     */
    public function testExists()
    {
        // When key doesn't exist, it should be false
        $this->assertFalse($this->cache->exists('key'));

        // When key does exist, it still should be false
        $this->cache->set('key', 'val');
        $this->assertFalse($this->cache->exists('key'));
    }

    /**
     * Test the get function
     */
    public function testGet()
    {
        // When key doesn't exist, it should be false
        $this->assertFalse($this->cache->get('key'));

        // When key does exist, it still should be false
        $this->cache->set('key', 'val');
        $this->assertFalse($this->cache->get('key'));
    }

    /**
     * Test the set function
     */
    public function testSet()
    {
        $this->assertFalse($this->cache->set('key', 'val'));
    }
}
