<?php

namespace FoodHygiene\Cache;

use \Monolog\Handler\NullHandler;
use \Monolog\Logger;
use \PHPUnit\Framework\TestCase;
use \Predis\Client;
use \Slim\Container;
use \ReflectionClass;

/**
 * Class RedisCacheTest
 * @package FoodHygiene\Cache
 */
class RedisCacheTest extends TestCase
{
    /**
     * @var RedisCache
     */
    private $cache;

    /**
     * @var mixed
     */
    private $predis_mock;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $container = new Container();

        $logger = new Logger('TestLogger');
        $logger->pushHandler(new NullHandler());
        $container['logger'] = $logger;

        $this->cache = CacheFactory::createCache('RedisCache', $container);

        $this->predis_mock = $this->getMockBuilder(Client::class)
            ->setMethods(array('exists', 'get', 'set', 'expireat'))
            ->getMock();

        // Change private property for predis client
        $reflection = new ReflectionClass($this->cache);
        $property = $reflection->getProperty('predis');
        $property->setAccessible(true);
        $property->setValue($this->cache, $this->predis_mock);
    }

    /**
     * Test the exists function
     */
    public function testExists()
    {
        // Configure the mocked predis client
        $this->predis_mock
            ->expects($this->exactly(2))
            ->method('exists')
            ->withConsecutive(array('key'), array('key'))
            ->will($this->onConsecutiveCalls(false, true));

        // When key doesn't exist, it should be false
        $this->assertFalse($this->cache->exists('key'));

        // When key does exist, it still should be true
        $this->cache->set('key', 'val');
        $this->assertTrue($this->cache->exists('key'));
    }

    /**
     * Test the get function
     */
    public function testGet()
    {
        // Configure the mocked predis client
        $this->predis_mock
            ->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(array('key'), array('key'))
            ->will($this->onConsecutiveCalls(false, 'val'));

        // When key doesn't exist, it should be false
        $this->assertFalse($this->cache->get('key'));

        // When key does exist, it still should be the value
        $this->cache->set('key', 'val');
        $this->assertEquals('val', $this->cache->get('key'));
    }

    /**
     * Test the set function
     */
    public function testSet()
    {
        // Configure the mocked predis client
        $this->predis_mock
            ->expects($this->once())
            ->method('get')
            ->with('key')
            ->willReturn('val');
        $this->predis_mock
            ->expects($this->once())
            ->method('set')
            ->with('key', 'val')
            ->willReturn(true);
        $this->predis_mock
            ->expects($this->never())
            ->method('expireat');

        $this->assertTrue($this->cache->set('key', 'val'));
        $this->assertEquals('val', $this->cache->get('key'));
    }

    /**
     * Test the set function with an expires time
     */
    public function testSetWithExpires()
    {
        // Configure the mocked predis client
        $plus_1_day = strtotime('+1 days');
        $this->predis_mock
            ->expects($this->once())
            ->method('get')
            ->with('key')
            ->willReturn('val');
        $this->predis_mock
            ->expects($this->once())
            ->method('set')
            ->with('key', 'val')
            ->willReturn(true);
        $this->predis_mock
            ->expects($this->once())
            ->method('expireat')
            ->with('key', $plus_1_day)
            ->willReturn(true);

        $this->assertTrue($this->cache->set('key', 'val', $plus_1_day));
        $this->assertEquals('val', $this->cache->get('key'));
    }
}
