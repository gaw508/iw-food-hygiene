<?php

namespace FoodHygiene\Cache;

use \PHPUnit\Framework\TestCase;
use \Slim\Container;

/**
 * Class CacheFactoryTest
 * @package FoodHygiene\Cache
 */
class CacheFactoryTest extends TestCase
{
    /**
     * test the create cache static method
     */
    public function testCreateCache()
    {
        $container = new Container();

        $cache = CacheFactory::createCache('NullCache', $container);
        $this->assertInstanceOf(CacheInterface::class, $cache);

        $cache = CacheFactory::createCache('NotACache', $container);
        $this->assertFalse($cache);
    }
}
