<?php

namespace FoodHygiene\Controller;

use FoodHygiene\Cache\CacheFactory;
use \FoodHygiene\ErrorHandler;
use FoodHygiene\FsaApiClient;
use Gaw508\Config\Config;
use \Monolog\Handler\NullHandler;
use \Monolog\Logger;
use \PHPUnit\Framework\TestCase;
use \Slim\Container;
use \Slim\Http\Headers;
use \Slim\Http\Request;
use \Slim\Http\Response;
use \Slim\Http\Stream;
use \Slim\Http\Uri;
use Slim\Views\Twig;

/**
 * Class RatingsControllerTest
 *
 * @package FoodHygiene\Controller
 */
class RatingsControllerTest extends TestCase
{
    /**
     * @var RatingsController
     */
    private $controller;

    /**
     * @var mixed
     */
    private $fsa_api_mock;

    /**
     * @var array
     */
    private $local_authority_list = array(
        array(
            'Name' => 'Ipswich',
            'LocalAuthorityId' => 101,
            'SchemeType' => 1
        ),
        array(
            'Name' => 'Glasgow',
            'LocalAuthorityId' => 102,
            'SchemeType' => 2
        )
    );

    private $establishment_list = array(
        array('RatingKey' => 'fhrs_exempt_en-gb', 'RatingValue' => 'Exempt'),
        array('RatingKey' => 'fhrs_5_en-gb', 'RatingValue' => '5')
    );

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $container = new Container();

        $error_handler = new ErrorHandler($container);
        $error_handler->register();

        $logger = new Logger('TestLogger');
        $logger->pushHandler(new NullHandler());
        $container['logger'] = $logger;

        $container['view'] = new Twig(
            dirname(dirname(__DIR__)) . '/frontend/templates'
        );

        Config::set('cache_expiry_days', 1);
        $container['cache'] = CacheFactory::createCache(
            'NullCache',
            $container
        );

        $this->fsa_api_mock = $this->getMockBuilder(FsaApiClient::class)
            ->disableOriginalConstructor()
            ->setMethods(array(
                'getLocalAuthorityList',
                'getLocalAuthorityByID',
                'getEstablishmentsByLocalAuthority'
            ))
            ->getMock();
        $container['fsa_api_client'] = $this->fsa_api_mock;

        $this->controller = new RatingsController($container);
    }

    /**
     * Test the distributions action with the root path
     */
    public function testDistributionsRootPath()
    {
        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityList')
            ->willReturn($this->local_authority_list);

        // Root path
        $response = $this->controller->distributions(
            $this->getRequest('/'),
            new Response(),
            array()
        );
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test the distributions action with a local authority
     */
    public function testDistributionsWithAuthority()
    {
        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityList')
            ->willReturn($this->local_authority_list);

        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityByID')
            ->with('101')
            ->willReturn($this->local_authority_list[0]);

        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getEstablishmentsByLocalAuthority')
            ->with('101')
            ->willReturn($this->establishment_list);

        // Authority in path - real authority
        $response = $this->controller->distributions(
            $this->getRequest('/authority/101'),
            new Response(),
            array('id' => '101')
        );
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test the distributions action with an unknown local authority
     */
    public function testDistributionsWithUnknownAuthority()
    {
        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityList')
            ->willReturn($this->local_authority_list);

        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityByID')
            ->with('1011')
            ->willReturn(false);

        // Authority in path - unknown authority
        $response = $this->controller->distributions(
            $this->getRequest('/authority/1011'),
            new Response(),
            array('id' => '1011')
        );
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Test the redirect action
     */
    public function testRedirect()
    {
        // With query
        $request = $this->getRequest('/authority', 'localAuthorityId=1');
        $response = $this->controller->redirect($request, new Response());
        $this->assertEquals(302, $response->getStatusCode());

        // With query
        $request = $this->getRequest('/authority');
        $response = $this->controller->redirect($request, new Response());
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Helper method for creating request objects
     *
     * @param $path
     * @param string $query
     * @return Request
     */
    private function getRequest($path, $query = '')
    {
        return new Request(
            'GET',
            new Uri('http', 'localhost', 80, $path, $query),
            new Headers(),
            array(),
            array(),
            new Stream(fopen(dirname(__DIR__) . '/data/body.txt', 'r'))
        );
    }
}
