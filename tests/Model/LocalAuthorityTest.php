<?php

namespace FoodHygiene\Model;

use \FoodHygiene\Cache\NullCache;
use \FoodHygiene\FsaApiClient;
use \FoodHygiene\Model\Scheme\FHISScheme;
use \FoodHygiene\Model\Scheme\FHRSScheme;
use \FoodHygiene\Model\Scheme\SchemeInterface;
use \PHPUnit\Framework\TestCase;
use \Slim\Container;
use \Monolog\Handler\NullHandler;
use \Monolog\Logger;
use \Exception;
use \Gaw508\Config\Config;

/**
 * Class LocalAuthorityTest
 * @package FoodHygiene\Model
 */
class LocalAuthorityTest extends TestCase
{
    /**
     * @var LocalAuthority
     */
    private $local_authority;

    /**
     * @var mixed
     */
    private $cache_mock;

    /**
     * @var mixed
     */
    private $fsa_api_mock;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $container = new Container();

        $logger = new Logger('TestLogger');
        $logger->pushHandler(new NullHandler());
        $container['logger'] = $logger;

        $this->cache_mock = $this->getMockBuilder(NullCache::class)
            ->disableOriginalConstructor()
            ->setMethods(array('exists', 'get', 'set'))
            ->getMock();
        $container['cache'] = $this->cache_mock;

        $this->fsa_api_mock = $this->getMockBuilder(FsaApiClient::class)
            ->disableOriginalConstructor()
            ->setMethods(array(
                'getLocalAuthorityList',
                'getLocalAuthorityByID',
                'getEstablishmentsByLocalAuthority'
            ))
            ->getMock();
        $container['fsa_api_client'] = $this->fsa_api_mock;

        $this->local_authority = new LocalAuthority($container);
    }

    /**
     * Test the getter methods
     */
    public function testGetters()
    {
        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityByID')
            ->with(123)
            ->willReturn(array(
                'Name' => 'Test Authority',
                'LocalAuthorityId' => 123,
                'SchemeType' => 1
            ));

        $this->local_authority->loadByID(123);

        $this->assertEquals(
            'Test Authority',
            $this->local_authority->getName()
        );
        $this->assertEquals(
            123,
            $this->local_authority->getID()
        );
        $this->assertInstanceOf(
            SchemeInterface::class,
            $this->local_authority->getScheme()
        );
    }

    /**
     * Test the get list method
     */
    public function testGetList()
    {
        $data = array(
            array(
                'Name' => 'Ipswich',
                'LocalAuthorityId' => 101,
                'SchemeType' => 1
            ),
            array(
                'Name' => 'Glasgow',
                'LocalAuthorityId' => 102,
                'SchemeType' => 2
            )
        );

        $this->fsa_api_mock
            ->expects($this->exactly(3))
            ->method('getLocalAuthorityList')
            ->will($this->onConsecutiveCalls(
                $data,
                array(),
                false
            ));

        // First response gives data
        $this->assertEquals($data, $this->local_authority->getList());

        // Second gives empty, but valid data
        $this->assertEquals(array(), $this->local_authority->getList());

        // Third should throw exception
        try {
            $this->local_authority->getList();
            $this->assertTrue(false);
        } catch (Exception $e) {
            $this->assertTrue(true);
        }
    }

    /**
     * Test the load by ID method
     */
    public function testLoadById()
    {
        $this->fsa_api_mock
            ->expects($this->exactly(3))
            ->method('getLocalAuthorityByID')
            ->withConsecutive(array(101), array(102), array(103))
            ->will($this->onConsecutiveCalls(
                array(
                    'Name' => 'Ipswich',
                    'LocalAuthorityId' => 101,
                    'SchemeType' => 1
                ),
                array(
                    'Name' => 'Glasgow',
                    'LocalAuthorityId' => 102,
                    'SchemeType' => 2
                ),
                false
            ));

        $this->local_authority->loadByID(101);
        $this->assertEquals(
            'Ipswich',
            $this->local_authority->getName()
        );
        $this->assertEquals(
            101,
            $this->local_authority->getID()
        );
        $this->assertInstanceOf(
            FHRSScheme::class,
            $this->local_authority->getScheme()
        );

        $this->local_authority->loadByID(102);
        $this->assertEquals(
            'Glasgow',
            $this->local_authority->getName()
        );
        $this->assertEquals(
            102,
            $this->local_authority->getID()
        );
        $this->assertInstanceOf(
            FHISScheme::class,
            $this->local_authority->getScheme()
        );

        $this->assertFalse(
            $this->local_authority->loadByID(103)
        );
    }

    /**
     * Test the get rating distribution method with no cached data
     */
    public function testGetRatingDistributionWithNoCache()
    {
        $scenario = array(
            'localAuthority' => array(
                'Name' => 'Ipswich',
                'LocalAuthorityId' => 101,
                'SchemeType' => 1
            ),
            'establishments' => array(
                array('RatingKey' => 'fhrs_exempt_en-gb', 'RatingValue' => 'Exempt'),
                array('RatingKey' => 'fhrs_5_en-gb', 'RatingValue' => '5')
            ),
            'distribution' => array(
                "5-star" => "50.0",
                "4-star" => "0.0",
                "3-star" => "0.0",
                "2-star" => "0.0",
                "1-star" => "0.0",
                "0-star" => "0.0",
                "Exempt" => "50.0",
                "Awaiting Inspection" => "0.0",
                "Awaiting Publication" => "0.0"
            ),
            'isCached' => false
        );

        $this->runTestGetRatingDistribution($scenario);
    }

    /**
     * Test the get rating distribution method with cached data
     */
    public function testGetRatingDistributionWithCache()
    {
        $scenario = array(
            'localAuthority' => array(
                'Name' => 'Glasgow',
                'LocalAuthorityId' => 102,
                'SchemeType' => 2
            ),
            'establishments' => array(
                array('ratingKey' => 'fhis_exempt_en-gb', 'ratingValue' => 'Exempt'),
                array('ratingKey' => 'fhis_pass_en-gb', 'ratingValue' => 'Pass')
            ),
            'distribution' => array(
                "Pass" => "50.0",
                "Pass and Eat Safe" => "0.0",
                "Improvement Required" => "0.0",
                "Exempt" => "50.0",
                "Awaiting Inspection" => "0.0",
                "Awaiting Publication" => "0.0"
            ),
            'isCached' => true
        );

        $this->runTestGetRatingDistribution($scenario);
    }

    /**
     * Test the get rating distribution method with failure from
     * FSA API
     */
    public function testGetRatingDistributionWithAPIFail()
    {
        $scenario = array(
            'localAuthority' => array(
                'Name' => 'Some Town',
                'LocalAuthorityId' => 103,
                'SchemeType' => 2
            ),
            'establishments' => false,
            'distribution' => false,
            'isCached' => false
        );

        $this->runTestGetRatingDistribution($scenario);
    }

    private function runTestGetRatingDistribution($scenario)
    {
        Config::set('cache_expiry_days', 1);

        $this->fsa_api_mock
            ->expects($this->once())
            ->method('getLocalAuthorityByID')
            ->with($scenario['localAuthority']['LocalAuthorityId'])
            ->willReturn($scenario['localAuthority']);

        $this->cache_mock
            ->expects($this->once())
            ->method('exists')
            ->with(
                'ratings_distribution::'
                . $scenario['localAuthority']['LocalAuthorityId']
            )
            ->willReturn($scenario['isCached']);

        if ($scenario['isCached']) {
            $this->cache_mock
                ->expects($this->once())
                ->method('get')
                ->with(
                    'ratings_distribution::'
                    . $scenario['localAuthority']['LocalAuthorityId']
                )
                ->willReturn(json_encode($scenario['distribution']));
        } else {
            $this->fsa_api_mock
                ->expects($this->once())
                ->method('getEstablishmentsByLocalAuthority')
                ->with($scenario['localAuthority']['LocalAuthorityId'])
                ->willReturn($scenario['establishments']);

            if ($scenario['establishments']) {
                $this->cache_mock
                    ->expects($this->once())
                    ->method('set')
                    ->with(
                        'ratings_distribution::'
                        . $scenario['localAuthority']['LocalAuthorityId'],
                        json_encode($scenario['distribution'])
                    )
                    ->willReturn(true);
            }
        }

        $this->local_authority->loadByID(
            $scenario['localAuthority']['LocalAuthorityId']
        );

        if ($scenario['establishments']) {
            $this->assertEquals(
                $scenario['distribution'],
                $this->local_authority->getRatingDistribution()
            );
        } else {
            try {
                $this->local_authority->getRatingDistribution();
                $this->assertTrue(false);
            } catch (Exception $e) {
                $this->assertTrue(true);
            }
        }
    }
}
