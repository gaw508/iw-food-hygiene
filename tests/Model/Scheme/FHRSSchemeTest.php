<?php

namespace FoodHygiene\Model\Scheme;

use \PHPUnit\Framework\TestCase;

/**
 * Class FHRSSchemeTest
 * @package FoodHygiene\Model\Scheme
 */
class FHRSSchemeTest extends TestCase
{
    /**
     * @var FHRSScheme
     */
    private $scheme;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $this->scheme = new FHRSScheme();
    }

    /**
     * Test the scheme type constant
     */
    public function testSchemeType()
    {
        $this->assertEquals(1, FHRSScheme::SCHEME_TYPE);
    }

    /**
     * Test the get allowed ratings method
     */
    public function testGetAllowedRatings()
    {
        $this->assertEquals(array(
            "5-star",
            "4-star",
            "3-star",
            "2-star",
            "1-star",
            "0-star",
            "Exempt",
            "Awaiting Inspection",
            "Awaiting Publication"
        ), $this->scheme->getAllowedRatings());
    }

    /**
     * test the map key/code to rating method
     */
    public function testMapCodeToRating()
    {
        // Check real codes
        $this->assertEquals(
            '5-star',
            $this->scheme->mapCodeToRating('fhrs_5_en-gb')
        );
        $this->assertEquals(
            'Exempt',
            $this->scheme->mapCodeToRating('fhrs_exempt_en-gb')
        );
        $this->assertEquals(
            'Awaiting Inspection',
            $this->scheme->mapCodeToRating(
                'fhrs_awaitinginspection_en-gb'
            )
        );

        // Check not real code
        $this->assertFalse($this->scheme->mapCodeToRating('BlahBlah'));

        // Check rating value
        $this->assertFalse($this->scheme->mapCodeToRating('Awaiting Inspection'));

        // Check from FHRS Scheme
        $this->assertFalse($this->scheme->mapCodeToRating('fhis_pass_en-gb'));
        $this->assertFalse($this->scheme->mapCodeToRating('fhis_exempt_en-gb'));
        $this->assertFalse($this->scheme->mapCodeToRating('fhis_awaiting_inspection_en-gb'));
    }
}
