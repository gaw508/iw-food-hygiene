<?php

namespace FoodHygiene\Model\Scheme;

use \PHPUnit\Framework\TestCase;

/**
 * Class FHISSchemeTest
 * @package FoodHygiene\Model\Scheme
 */
class FHISSchemeTest extends TestCase
{
    /**
     * @var FHISScheme
     */
    private $scheme;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $this->scheme = new FHISScheme();
    }

    /**
     * Test the scheme type constant
     */
    public function testSchemeType()
    {
        $this->assertEquals(2, FHISScheme::SCHEME_TYPE);
    }

    /**
     * Test the get allowed ratings method
     */
    public function testGetAllowedRatings()
    {
        $this->assertEquals(array(
            "Pass",
            "Pass and Eat Safe",
            "Improvement Required",
            "Exempt",
            "Awaiting Inspection",
            "Awaiting Publication"
        ), $this->scheme->getAllowedRatings());
    }

    /**
     * test the map key/code to rating method
     */
    public function testMapCodeToRating()
    {
        // Check real codes
        $this->assertEquals(
            'Pass',
            $this->scheme->mapCodeToRating('fhis_pass_en-gb')
        );
        $this->assertEquals(
            'Exempt',
            $this->scheme->mapCodeToRating('fhis_exempt_en-gb')
        );
        $this->assertEquals(
            'Awaiting Inspection',
            $this->scheme->mapCodeToRating(
                'fhis_awaiting_inspection_en-gb'
            )
        );

        // Check not real code
        $this->assertFalse($this->scheme->mapCodeToRating('BlahBlah'));

        // Check rating value
        $this->assertFalse($this->scheme->mapCodeToRating('Pass and Eat Safe'));

        // Check from FHRS Scheme
        $this->assertFalse($this->scheme->mapCodeToRating('fhrs_2_en-gb'));
        $this->assertFalse($this->scheme->mapCodeToRating('fhrs_exempt_en-gb'));
        $this->assertFalse($this->scheme->mapCodeToRating('fhrs_awaitinginspection_en-gb'));
    }
}
