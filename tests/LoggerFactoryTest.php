<?php

namespace FoodHygiene;

use \Gaw508\Config\Config;
use \Monolog\Logger;
use \PHPUnit\Framework\TestCase;

/**
 * Class LoggerFactoryTest
 *
 * @package FoodHygiene
 */
class LoggerFactoryTest extends TestCase
{
    /**
     * @var LoggerFactory
     */
    private $factory;

    /**
     * Set up before each test
     */
    public function setUp()
    {
        $this->factory = new LoggerFactory();
    }

    /**
     * Test the create logger method
     */
    public function testCreateLogger()
    {
        Config::set('log_name', 'Testing');
        Config::set('LOG_DIR', dirname(__DIR__) . '/tmp/logs');
        Config::set('max_log_files', 10);
        Config::set('log_level', 100);

        Config::set('remote_udp_logging_enabled', false);
        $logger_1 = $this->factory->createLogger();
        $this->assertInstanceOf(Logger::class, $logger_1);
        $this->assertEquals('Testing', $logger_1->getName());
        $this->assertEquals(1, count($logger_1->getHandlers()));

        Config::set('remote_udp_logging_enabled', true);
        Config::set('remote_udp_logging_host', 'localhost');
        Config::set('remote_udp_logging_port', 59010);
        $logger_2 = $this->factory->createLogger();
        $this->assertInstanceOf(Logger::class, $logger_2);
        $this->assertEquals('Testing', $logger_2->getName());
        $this->assertEquals(2, count($logger_2->getHandlers()));
    }
}
