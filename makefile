docker-build:
	docker build -t gwebb-food-hygiene .

docker-run:
	docker run -d --name gwebb-food-hygiene -p 8081:80 gwebb-food-hygiene

docker-stop-rm:
	docker container stop gwebb-food-hygiene && docker container rm gwebb-food-hygiene

install:
	composer install && composer update && npm install && gulp

test:
	composer test
