<?php

/**
 * Entry point for http requests to the application
 */

require_once dirname(__DIR__) . '/config/bootstrap.php';

use \Gaw508\Config\Config;
use \Slim\App;
use \Slim\Container;
use \Slim\Views\Twig;
use \FoodHygiene\ErrorHandler;
use \FoodHygiene\LoggerFactory;
use \FoodHygiene\FsaApiClient;
use \FoodHygiene\Cache\CacheFactory;

$container = new Container();

// View renderer service
$container['view'] = new Twig(Config::get('TEMPLATE_DIR'));

// Logging configuration
$container['logger'] = (new LoggerFactory())->createLogger();

// FSA API Client configuration
$container['fsa_api_client'] = new FsaApiClient($container);

// Cache client
$container['cache'] = CacheFactory::createCache(
    Config::get('cache_type'),
    $container
);

// Custom error handling
$error_handler = new ErrorHandler($container);
$error_handler->register();

// Start app and define routes
$app = new App($container);
$app->get('/', '\FoodHygiene\Controller\RatingsController:distributions');
$app->get('/authority/{id}', '\FoodHygiene\Controller\RatingsController:distributions');
$app->get('/authority', '\FoodHygiene\Controller\RatingsController:redirect');
$app->run();
