<?php

namespace FoodHygiene;

use \Exception;
use \Psr\Container\ContainerInterface as Container;
use \GuzzleHttp\Client as GuzzleClient;
use \Gaw508\Config\Config;

/**
 * Class FsaApiClient
 *
 * The API Client for the Food Standards Agency API V2
 *
 * @package FoodHygiene
 */
class FsaApiClient
{
    /**
     * Slim DI Container
     *
     * @var Container
     */
    private $container;

    /**
     * @var GuzzleClient
     */
    private $guzzle;

    /**
     * FsaApiClient constructor.
     *
     * @param Container $container  Slim DI Container
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->guzzle = new GuzzleClient(array(
            'base_uri' => Config::get('fsa_api_base_uri'),
            'timeout'  => Config::get('fsa_api_timeout'),
            'headers' => array(
                'x-api-version' => Config::get('fsa_api_version')
            )
        ));
    }

    /**
     * Fetches list of all local authorities
     *
     * @return array|bool   list of local authorities or false
     */
    public function getLocalAuthorityList()
    {
        $response = $this->callApi('/Authorities/basic');

        if (!$response) {
            return false;
        }

        if (!array_key_exists('authorities', $response)) {
            $this->container->logger->error(
                'Invalid response from FSA API'
            );
            return false;
        }

        return $response['authorities'];
    }

    /**
     * Get a local authority by it's ID
     *
     * @param int $local_authority_id   The ID of the local authority
     * @return array|bool               Data for the local authority
     */
    public function getLocalAuthorityByID($local_authority_id)
    {
        return $this->callApi("/Authorities/$local_authority_id");
    }

    /**
     * Get a list of establishments for a local authority by it's ID
     *
     * Max limit on any page of data is 5000, which could easily be
     * exceeded with this data. Because of this, this handles
     * calling each page of data, and merges them together.
     *
     * @param int $local_authority_id   The ID of the local authority
     * @return array|bool               List of establishments
     */
    public function getEstablishmentsByLocalAuthority($local_authority_id)
    {
        // Total pages will be updated after first response with actual number of pages.
        $total_pages = 1;
        $current_page = 1;
        $establishments = array();

        // Loop through all pages of data in API
        while ($current_page <= $total_pages) {
            $response = $this->callApi("/Establishments", array(
                'query' => array(
                    'localAuthorityId' => $local_authority_id,
                    'pageSize' => 0,
                    'pageNumber' => $current_page
                )
            ));

            if (!$response) {
                return false;
            }

            if (!array_key_exists('establishments', $response)
                || !array_key_exists('meta', $response)) {
                $this->container->logger->error(
                    'Invalid response from FSA API'
                );
                return false;
            }

            $establishments = array_merge(
                $establishments,
                $response['establishments']
            );

            // Update the total pages so the loop continues if necessary
            $total_pages = $response['meta']['totalPages'];

            $current_page++;
        }

        return $establishments;
    }

    /**
     * Call the FSA API
     *
     * @param string $url       The request URL
     * @param array $options    The request options
     * @return bool|array       False on failure, array of data otherwise
     */
    private function callApi($url, $options = array())
    {
        try {
            $response = $this->guzzle->get($url, $options);

            if ($response->getStatusCode() !== 200) {
                $this->container->logger->error(
                    'Request to FSA API failed with status'
                    . $response->getStatusCode()
                );
                return false;
            }

            $response = json_decode($response->getBody(), true);

            if (!is_array($response)) {
                $this->container->logger->error(
                    'Invalid response from FSA API'
                );
                return false;
            }

            return $response;
        } catch (Exception $e) {
            $this->container->logger->error(
                "FSA API Failure: {$e->getMessage()}"
            );
            return false;
        }
    }
}
