<?php

namespace FoodHygiene\Model\Scheme;

/**
 * Class FHRSScheme
 *
 * Implementation of the Food Hygiene Information Scheme in Scotland
 *
 * @package FoodHygiene\Model\Scheme
 */
class FHISScheme implements SchemeInterface
{
    /**
     * The value of the Scheme Type for FHIS
     */
    const SCHEME_TYPE = 2;

    /**
     * Get a list of Allowed ratings for scheme
     *
     * @return array    List of ratings allowed for scheme
     */
    public function getAllowedRatings()
    {
        return array(
            "Pass",
            "Pass and Eat Safe",
            "Improvement Required",
            "Exempt",
            "Awaiting Inspection",
            "Awaiting Publication"
        );
    }

    /**
     * Maps a code from API to rating to display
     *
     * @param string $code  code from API
     * @return bool|string  the rating or false if code isn't valid
     */
    public function mapCodeToRating($code)
    {
        $map = array(
            "fhis_pass_en-gb" => "Pass",
            "fhis_pass_and_eat_safe_en-gb" => "Pass and Eat Safe",
            "fhis_improvement_required_en-gb" => "Improvement Required",
            "fhis_exempt_en-gb" => "Exempt",
            "fhis_awaiting_inspection_en-gb" => "Awaiting Inspection",
            "fhis_awaiting_publication_en-gb" => "Awaiting Publication"
        );

        return array_key_exists($code, $map)
            ? $map[$code] : false;
    }
}
