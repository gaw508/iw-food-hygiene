<?php

namespace FoodHygiene\Model\Scheme;

/**
 * Class FHRSScheme
 *
 * Implementation of the Food Hygiene Rating Scheme in England, Wales and Northern Ireland
 *
 * @package FoodHygiene\Model\Scheme
 */
class FHRSScheme implements SchemeInterface
{
    /**
     * The value of the Scheme Type for FHRS
     */
    const SCHEME_TYPE = 1;

    /**
     * Get a list of Allowed ratings for scheme
     *
     * @return array    List of ratings allowed for scheme
     */
    public function getAllowedRatings()
    {
        return array(
            "5-star",
            "4-star",
            "3-star",
            "2-star",
            "1-star",
            "0-star",
            "Exempt",
            "Awaiting Inspection",
            "Awaiting Publication"
        );
    }

    /**
     * Maps a code from API to rating to display
     *
     * @param string $code  code from API
     * @return bool|string  the rating or false if code isn't valid
     */
    public function mapCodeToRating($code)
    {
        $map = array(
            "fhrs_5_en-gb" => "5-star",
            "fhrs_4_en-gb" => "4-star",
            "fhrs_3_en-gb" => "3-star",
            "fhrs_2_en-gb" => "2-star",
            "fhrs_1_en-gb" => "1-star",
            "fhrs_0_en-gb" => "0-star",
            "fhrs_exempt_en-gb" => "Exempt",
            "fhrs_awaitinginspection_en-gb" => "Awaiting Inspection",
            "fhrs_awaitingpublication_en-gb" => "Awaiting Publication"
        );

        return array_key_exists($code, $map)
            ? $map[$code] : false;
    }
}
