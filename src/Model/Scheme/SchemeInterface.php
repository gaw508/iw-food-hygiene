<?php

namespace FoodHygiene\Model\Scheme;

/**
 * Interface SchemeInterface
 *
 * Provides interface of Food Hygiene Scheme
 *
 * @package FoodHygiene\Model\Scheme
 */
interface SchemeInterface
{
    /**
     * Get a list of Allowed ratings for scheme
     *
     * @return array    List of ratings allowed for scheme
     */
    public function getAllowedRatings();

    /**
     * Maps a code from API to rating to display
     *
     * @param string $code  code from API
     * @return bool|string  the rating or false if code isn't valid
     */
    public function mapCodeToRating($code);
}
