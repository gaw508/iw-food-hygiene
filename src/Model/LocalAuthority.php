<?php

namespace FoodHygiene\Model;

use \FoodHygiene\Model\Scheme\FHRSScheme;
use \FoodHygiene\Model\Scheme\FHISScheme;
use \Exception;
use \FoodHygiene\Model\Scheme\SchemeInterface;
use \Gaw508\Config\Config;
use \Psr\Container\ContainerInterface as Container;

/**
 * Class LocalAuthority
 *
 * Model of a LocalAuthority, agnostic to a Scheme
 *
 * @package FoodHygiene\Model
 */
class LocalAuthority
{
    /**
     * Slim DI Container
     *
     * @var Container
     */
    private $container;

    /**
     * The local authority ID
     *
     * @var int
     */
    private $id;

    /**
     * The name of the local authority
     *
     * @var string
     */
    private $name;

    /**
     * The Scheme Object for the local authority
     *
     * @var SchemeInterface
     */
    private $scheme;

    /**
     * LocalAuthority constructor.
     *
     * @param Container $container  Slim DI Container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Get the id of the local authority
     *
     * @return int
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * Get the name of the local authority
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the scheme of the local authority
     *
     * @return SchemeInterface
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * Get a list of all local authorities under the FSA
     *
     * @return array        List of local authorities
     * @throws Exception    If failure to get list data
     */
    public function getList()
    {
        $local_authorities = $this->container->fsa_api_client
            ->getLocalAuthorityList();

        if (!is_array($local_authorities)) {
            throw new Exception(
                'Invalid data from FSA API in LocalAuthority::getList'
            );
        }

        return $local_authorities;
    }

    /**
     * Loads data into current instance for a local authority of a given ID
     *
     * @param int $local_authority_id   The ID of the local authority
     * @return bool                     Whether or not authority was found
     * @throws Exception                If no valid local authority is found
     */
    public function loadByID($local_authority_id)
    {
        $local_authority = $this->container->fsa_api_client
            ->getLocalAuthorityByID($local_authority_id);

        if (!is_array($local_authority)
            || !array_key_exists('LocalAuthorityId', $local_authority)
            || !array_key_exists('Name', $local_authority)
            || !array_key_exists('SchemeType', $local_authority)) {
            return false;
        }

        $this->id = $local_authority['LocalAuthorityId'];
        $this->name = $local_authority['Name'];
        $this->scheme = $this->getSchemeFromType(
            $local_authority['SchemeType']
        );

        return true;
    }

    /**
     * Gets an instance of SchemeInterface based on the scheme type
     *
     * @param int $scheme_type  The Scheme Type
     * @return SchemeInterface  The Scheme Object
     * @throws Exception        If an unknown scheme type is given
     */
    private function getSchemeFromType($scheme_type)
    {
        switch ($scheme_type) {
            case FHRSScheme::SCHEME_TYPE:
                return new FHRSScheme();
            case FHISScheme::SCHEME_TYPE:
                return new FHISScheme();
            default:
                throw new Exception('Unknown scheme');
        }
    }

    /**
     * Gets distribution of ratings for all allowed ratings within
     * this local authority
     *
     * @return array        The distribution of ratings, in percent
     * @throws Exception    If failure to get establishment data
     */
    public function getRatingDistribution()
    {
        // Try getting from cache
        $cache_key = "ratings_distribution::$this->id";
        $cached_distribution = $this->getRatingDistributionFromCache($cache_key);
        if ($cached_distribution) {
            return $cached_distribution;
        }

        // Else use API
        $api_distribution = $this->getRatingDistributionFromAPI();

        // Save to cache
        $this->container->cache->set(
            $cache_key,
            json_encode($api_distribution),
            strtotime('+' . Config::get('cache_expiry_days') . ' days')
        );

        return $api_distribution;
    }

    /**
     * Check the cache for valid distribution for this authority
     *
     * @param string $cache_key     The cache key for this auth
     * @return bool|array           False or the distribution
     */
    private function getRatingDistributionFromCache($cache_key)
    {
        if (!$this->container->cache->exists($cache_key)) {
            return false;
        }

        return json_decode(
            $this->container->cache->get($cache_key),
            true
        );
    }

    /**
     * Get the establishments from the API and calculate
     * a distribution for that local authority
     *
     * @return array        The distribution of ratings
     * @throws Exception    If the data from the API is invalid
     */
    private function getRatingDistributionFromAPI()
    {
        $establishments = $this->container->fsa_api_client
            ->getEstablishmentsByLocalAuthority($this->id);

        if (!is_array($establishments)) {
            throw new Exception(
                'Invalid data from FSA API in LocalAuthority::getRatingDistribution'
            );
        }

        list($rating_establishment_counts, $total_establishments)
            = $this->getRatingEstablishmentCounts($establishments);

        return $this->calculateDistributionFromCounts(
            $rating_establishment_counts,
            $total_establishments
        );
    }

    /**
     * Get list of ratings with the count of establishments in each
     *
     * @param array $establishments     List of establishments
     * @return array                    List of ratings with counts
     */
    private function getRatingEstablishmentCounts($establishments)
    {
        $bins = array_fill_keys($this->scheme->getAllowedRatings(), 0);
        $total = 0;
        foreach ($establishments as $establishment) {
            $rating = $this->scheme->mapCodeToRating(
                $establishment['RatingKey']
            );

            if (!$rating) {
                $this->container->logger->warning(
                    "Rating not allowed: {$establishment['RatingKey']}"
                );
                continue;
            }

            $bins[$rating]++;
            $total++;
        }
        return array($bins, $total);
    }

    /**
     * Calculate distribution as a percentage from list of
     * ratings with counts of establishments
     *
     * @param array $bins   The list of ratings with counts
     * @param int $total    The total number of establishments
     * @return array        The distribution by rating
     */
    private function calculateDistributionFromCounts($bins, $total)
    {
        $distribution = array();
        foreach ($bins as $rating => $count) {
            $distribution[$rating] = number_format($count / $total * 100, 1);
        }
        return $distribution;
    }
}
