<?php

namespace FoodHygiene;

use Gaw508\Config\Config;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\SyslogUdpHandler;

/**
 * Class LoggerFactory
 *
 * Creates loggers based off application configuration
 *
 * @package FoodHygiene
 */
class LoggerFactory
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Create a logger
     *
     * @return Logger
     */
    public function createLogger()
    {
        $this->logger = new Logger(Config::get('log_name'));
        $this->addLogFileHandler();

        if (Config::get('remote_udp_logging_enabled')) {
            $this->addRemoteUdpHandler();
        }

        return $this->logger;
    }

    /**
     * Adds a log file handler to the logger
     */
    private function addLogFileHandler()
    {
        $this->logger->pushHandler(new RotatingFileHandler(
            Config::get('LOG_DIR') . '/app.log',
            Config::get('max_log_files'),
            Config::get('log_level')
        ));
    }

    /**
     * Adds a remote UDP syslog handler to logger
     */
    private function addRemoteUdpHandler()
    {
        $this->logger->pushHandler(new SyslogUdpHandler(
            Config::get('remote_udp_logging_host'),
            Config::get('remote_udp_logging_port'),
            LOG_USER,
            Config::get('log_level')
        ));
    }
}
