<?php

namespace FoodHygiene;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface as Container;

/**
 * Class ErrorHandler
 *
 * Custom error handlers
 *
 * @package FoodHygiene
 */
class ErrorHandler
{
    /**
     * @var Container
     */
    private $container;

    /**
     * ErrorHandler constructor.
     *
     * @param Container $container  Slim DI container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Register handlers with Slim
     */
    public function register()
    {
        $error_handler = $this;
        $this->container['errorHandler'] = function () use ($error_handler) {
            return array($error_handler, 'handleError');
        };
        $this->container['phpErrorHandler'] = function () use ($error_handler) {
            return array($error_handler, 'handleError');
        };
        $this->container['notFoundHandler'] = function () use ($error_handler) {
            return array($error_handler, 'handleNotFound');
        };
        $this->container['notAllowedHandler'] = function () use ($error_handler) {
            return array($error_handler, 'handleNotAllowed');
        };
    }

    /**
     * @param Request $request      Request Object
     * @param Response $response    Response Object
     * @param mixed $err            The error or exception
     */
    public function handleError(
        Request $request,
        Response $response,
        $err
    ) {
        $this->container->logger->error($err->getMessage());
        $response = $response->withStatus(500);
        return $this->container['view']->render($response, 'error.twig');
    }

    /**
     * @param Request $request      Request Object
     * @param Response $response    Response Object
     */
    public function handleNotFound(
        Request $request,
        Response $response
    ) {
        $this->container->logger->info("404 Not Found: {$request->getUri()}");
        $response = $response->withStatus(404);
        return $this->container->view->render($response, 'not-found.twig');
    }

    /**
     * @param Request $request      Request Object
     * @param Response $response    Response Object
     * @param mixed $methods        The allowed methods
     */
    public function handleNotAllowed(
        Request $request,
        Response $response,
        $methods
    ) {
        $this->container->logger->info(
            "405 Method Not Allowed: {$request->getUri()} {$request->getMethod()}"
        );
        $response = $response
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods));
        return $this->container->view->render($response, 'not-allowed.twig');
    }
}
