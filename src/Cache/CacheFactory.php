<?php

namespace FoodHygiene\Cache;

use Psr\Container\ContainerInterface as Container;

/**
 * Class CacheFactory
 *
 * Creates implementations of Cache Interface
 *
 * @package FoodHygiene\Cache
 */
class CacheFactory
{
    /**
     * Returns a Cache
     *
     * @param string $cache_type    The cache type
     * @param Container $container  The DI container
     * @return CacheInterface|bool  Cache, or false
     */
    public static function createCache($cache_type, $container)
    {
        $class_name = "\\FoodHygiene\\Cache\\$cache_type";

        if (!class_exists($class_name)) {
            return false;
        }

        return new $class_name($container);
    }
}
