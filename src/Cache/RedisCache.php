<?php

namespace FoodHygiene\Cache;

use Psr\Container\ContainerInterface as Container;
use \Gaw508\Config\Config;
use \Predis\Client as PredisClient;
use \Exception;

/**
 * Class RedisCache
 *
 * Implementation of the cache interface for
 * a redis cache.
 *
 * @package FoodHygiene\Cache
 */
class RedisCache implements CacheInterface
{
    /**
     * DI Container
     *
     * @var Container
     */
    private $container;

    /**
     * The predis client
     *
     * @var PredisClient|bool
     */
    private $predis;

    /**
     * RedisCache constructor.
     *
     * @param Container $container
     */
    public function __construct($container)
    {
        $this->container = $container;
        try {
            $this->predis = new PredisClient(array(
                'host' => Config::get('redis_host'),
                'port' => Config::get('redis_port'),
            ));
        } catch (Exception $e) {
            $this->container->logger->error(
                "Failed to connect to redis: {$e->getMessage()}"
            );
            $this->predis = false;
        }
    }

    /**
     * Check if a key exists in the cache
     *
     * @param string $key   The key to check
     * @return bool         Whether or not it exists
     */
    public function exists($key)
    {
        if (!$this->predis) {
            return false;
        }

        try {
            return $this->predis->exists($key);
        } catch (Exception $e) {
            $this->container->logger->error(
                "Failed to check if redis key exists: {$e->getMessage()}"
            );
            return false;
        }
    }

    /**
     * Get a key from the cache
     *
     * @param string $key   The key to get the value of
     * @return string|bool  The success of operation
     */
    public function get($key)
    {
        if (!$this->predis) {
            return false;
        }

        try {
            return $this->predis->get($key);
        } catch (Exception $e) {
            $this->container->logger->error(
                "Failed to get redis key: {$e->getMessage()}"
            );
            return false;
        }
    }

    /**
     * Set a value of a key in the cache
     *
     * @param string $key       The key to set the value of
     * @param string $value     The value to set
     * @param bool $expires     The optional unix timestamp of expiry
     * @return bool             The success of operation
     */
    public function set($key, $value, $expires = false)
    {
        if (!$this->predis) {
            return false;
        }

        try {
            $this->predis->set($key, $value);
            if ($expires) {
                $this->predis->expireat($key, $expires);
            }
            return true;
        } catch (Exception $e) {
            $this->container->logger->error(
                "Failed to set redis key: {$e->getMessage()}"
            );
            return false;
        }
    }
}
