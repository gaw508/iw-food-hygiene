<?php

namespace FoodHygiene\Cache;

use \Psr\Container\ContainerInterface as Container;

/**
 * Class NullCache
 *
 * Null implementation of the Cache Interface
 *
 * @package FoodHygiene\Cache
 */
class NullCache implements CacheInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * NullCache constructor.
     *
     * @param Container $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Check if a key exists in the cache
     *
     * @param string $key   The key to check
     * @return bool         Whether or not it exists
     */
    public function exists($key)
    {
        return false;
    }

    /**
     * Get a key from the cache
     *
     * @param string $key   The key to get the value of
     * @return string|bool  The success of operation
     */
    public function get($key)
    {
        return false;
    }

    /**
     * Set a value of a key in the cache
     *
     * @param string $key       The key to set the value of
     * @param string $value     The value to set
     * @param bool $expires     The optional unix timestamp of expiry
     * @return bool             The success of operation
     */
    public function set($key, $value, $expires = false)
    {
        return false;
    }
}
