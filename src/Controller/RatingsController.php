<?php

namespace FoodHygiene\Controller;

use Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \FoodHygiene\Model\LocalAuthority;

/**
 * Class RatingsController
 *
 * Controller for route actions relating to ratings
 *
 * @package FoodHygiene\Controller
 */
class RatingsController
{
    /**
     * @var Container
     */
    private $container;

    /**
     * RatingsController constructor.
     *
     * @param Container $container  Slim DI container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Action for the distributions routes
     *
     * @param Request $request      The Request Object
     * @param Response $response    The Response Object
     * @param array $args           List of arguments passed in URL
     * @return Response             The Response Object
     */
    public function distributions(Request $request, Response $response, $args)
    {
        $local_authority = new LocalAuthority($this->container);
        $local_authorities = $local_authority->getList();

        $distribution = false;
        if (!empty($args['id'])) {
            $local_authority_id = (int) preg_replace(
                "/[^0-9]/",
                '',
                $args['id']
            );

            if (!$local_authority->loadByID($local_authority_id)) {
                $notFoundHandler = $this->container->get('notFoundHandler');
                return $notFoundHandler($request, $response);
            }

            $distribution = $local_authority->getRatingDistribution();
        }

        $response = $this->container->view->render(
            $response,
            'ratings-distribution.twig',
            array(
                'local_authorities' => $local_authorities,
                'distribution' => $distribution,
                'selected_local_authority_id' => $local_authority->getID(),
                'selected_local_authority_name' => $local_authority->getName()
            )
        );
        return $response;
    }

    /**
     * Action for handling redirection of form submission
     * to ensure friendly URL is used
     *
     * @param Request $request      The Request Object
     * @param Response $response    The Response Object
     * @return Response             The Response Object
     */
    public function redirect(Request $request, Response $response)
    {
        $localAuthorityId = $request->getQueryParam('localAuthorityId');

        if (!$localAuthorityId) {
            $notFoundHandler = $this->container->get('notFoundHandler');
            return $notFoundHandler($request, $response);
        }

        return $response
            ->withStatus(302)
            ->withHeader('Location', "/authority/$localAuthorityId");
    }
}
