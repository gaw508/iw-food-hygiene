# Food Hygiene Rating Distributions

This application gives the distribution of ratings for local 
authorities operating FSA FHRS and FHIS Schemes.

This was developed by George Webb as part of a technical test 
for Infinity Works.

## Solution overview

My chosen solution for this problem was to build a server-side 
application with data caching. My first thought was to build a 
completely client-side application, using a framework such as
Angular or React, however after seeing the performance of the 
FSA API I decided that a server-side application which cached 
distributions was better. The reasons for this are:

1. The responses from the FSA API for establishments within
a local authority area are very slow - ranging from 2-10 
seconds. Using a cache would avoid users having to wait for 
10 seconds after selecting a local authority.

2. The data being used and being shown is not urgent or
critical, and isn't updated at a high frequency, caching is
appropriate. The default cache expiry is one day.

The app is written mostly in PHP, with some HTML and CSS
for the front end parts. The PHP is built around the Slim 3
framework, utilising several other components and services.
The HTML components were writted using the Twig template
language, and utilises the Bootstrap framework.

The app was written using BDD, facilitated by Behat. Features 
were defined, and Behat tests written, in the *features*
directory. In addition to the BDD tests, there is a 
comprehensive set of PHPUnit tests within the *tests* 
directory. These, together with a couple of other tools can be
run collectively by running the either `make test` or 
`composer test` commands (note that `make install` or 
`composer install must be run first.)

The data caching within the application utilises **Redis**, 
this is not required and the application runs without it by 
default, but to get the intended experience it is recommended. 
To configure caching with Redis, you will need to change the 
configuration values:

* cache_type: RedisCache
* redis_host: [REDIS HOST]
* redis_port: [REDIS PORT]

When caching is enabled, the first time any given local 
authority is viewed in any given day, the request takes a bit 
longer that normal. This is because the data for that local
authority isn't cached yet, and that first view gets the data 
from the API and it is then cached for subsequent users.

A Dockerfile has been written which allows the application to 
be run in docker very easily. To use this, see the section
**Running in docker** below. Note that the Redis cache isn't
run as part of this container, and that will need to be 
configured as a separate service.

## Assumptions made

* Only modern browsers need to be supported. This includes:
    * Chrome (MacOS, Windows, iOS and Android) latest and -2 versions
    * Safari (MacOS, iOS) latest and -2 versions
    * Internet explorer 11 (Windows)
    * MS Edge (Windows) latest and -2 versions
    * Firefox (Windows and MacOS) latest and -2 versions

* The nature of the data is that it doesn't change at a high 
  frequency therefore, it is OK for the data to be cached, and
  potentially out of date by a day.
  
* The ratings for the FHRS (England, Wales, NI) can contain 
  the following ratings, and additional ratings should be 
  discarded:
    * 5-star
    * 4-star
    * 3-star
    * 2-star
    * 1-star
    * 0-star
    * Exempt
    * Awaiting Inspection
    * Awaiting Publication

* The ratings for the FHIS (Scotland) can contain the following 
  ratings, and additional ratings should be discarded:
    * Pass
    * Pass and Eat Safe
    * Improvement Required
    * Exempt
    * Awaiting Inspection
    * Awaiting Publication

## Potential improvements

A few potential improvements for future iterations:

* Having a nightly schedules job which pre-populates the cache
with the API data. This removes the "unlucky" first user having 
slower response times than subsequent users.

* Storing metrics for analytics and monitoring.

## Running in docker

**NOTE:** Additional steps are required to set up caching. 
Application will still work without, but getting distributions 
will be slower.

Build and run a docker container

    $ make docker-build
    $ make docker-run

Then access http://DOCKER_HOST:8081/

## Running tests

    $ make install
    $ make test
