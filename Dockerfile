FROM php:7.1-apache

# OS updates and packages
RUN apt-get update
RUN apt-get upgrade
RUN apt-get install -y zlib1g-dev unzip build-essential

# Extra PHP extensions
RUN docker-php-ext-install sockets
RUN docker-php-ext-install zip

# Copy in files
COPY . /var/www/food-hygiene

# Install composer
RUN curl https://getcomposer.org/installer --output installer
RUN php installer
RUN mv composer.phar /usr/bin/composer
RUN chmod +x /usr/bin/composer

# Run composer install and update
RUN cd /var/www/food-hygiene && composer install && composer update

# Install Node + NPM
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs

# Run frontend build
RUN cd /var/www/food-hygiene && npm install && npm install gulp -g && gulp

# Change doc root
ENV APACHE_DOCUMENT_ROOT /var/www/food-hygiene/web
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Enable mod rewrite
RUN a2enmod rewrite

# Log file permissions
RUN chown -R www-data:www-data /var/www/food-hygiene/tmp/log
