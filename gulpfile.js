'use strict';

/**
 * Gulpfile
 *
 * For building of front-end assets
 *
 * @type {*|Gulp}
 */

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css');

/**
 * Build CSS
 */
gulp.task('css', function () {
    gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'frontend/css/*.css'
    ])
        .pipe(minifyCSS())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('web/dist'));
});

/**
 * Default task
 */
gulp.task('default', ['css']);

/**
 * Watch for changes and re-run build
 */
gulp.task('watch', function () {
    gulp.run('default');

    gulp.watch('frontend/css/**/*.css', function (event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('css');
    });
});
